import os
import re 
import sys
import json
import time
import getopt
import random
import requests


# Set variables 
bearer = os.getenv('BEARER') # requires env variable for your API token
bind = 0
delete_records = 0
generate_records = 0
zoneid = ''
zonename = ''
prompt = 1


# Heredoc for BIND file
bind_template="""
;;
;; Domain:     {zonename}.
;;
;; This file is intended for use for informational and archival
;; purposes ONLY and MUST be edited before use on a production
;; DNS server.  In particular, you must:
;;   -- update the SOA record with the correct authoritative name server
;;   -- update the SOA record with the contact e-mail address information
;;   -- update the NS record(s) with the authoritative name servers for this domain.
;;
;; For further information, please consult the BIND documentation
;; located on the following website:
;;
;; http://www.isc.org/
;;
;; And RFC 1035:
;;
;; http://www.ietf.org/rfc/rfc1035.txt
;;
;; Please note that we do NOT offer technical support for any use
;; of this zone data, the BIND name server, or any other third-party
;; DNS software.
;;
;; Use at your own risk.

;; SOA Record
{zonename}.	3600	IN	SOA	{zonename}. root.{zonename}. 2032656183 7200 3600 86400 3600

;; A Records
"""



# Define script usage
def usage():
    print('usage:')
    print('-b --bind <integer> (generate zone files and upload)')
    print('-d --delete (deletes all dns records for given zone)')
    print('-g --generate <integer> (generate individual records using API)')
    print('-h --help')
    print('-y --yes (no prompt)')
    print('-z --zoneid <zoneId>')
    sys.exit()

# Get user input
try:
    opts, args = getopt.getopt(sys.argv[1:], 'g:db:hyz:', ['generate=', 'delete', 'bind=' ,'help', 'yes' ,'zoneid='])
except getopt.GetoptError:
    usage()

for opt, arg in opts:
    if opt in ('-h', '--help'):
        usage()
    elif opt in ('-g', '--generate'):
        generate_records = arg
    elif opt in ('-d', '--delete'):
        delete_records = 1
    elif opt in ('-z', '--zoneid'):
        zoneid = arg
    elif opt in ('-y', '--yes'):
        prompt = 0
    elif opt in ('-b', '--bind'):
        try:
            bind = int(arg)
        except:
            usage()
    else:
        usage()

# Function to prompt for yes or no 
def yes_or_no(question):
    ans = 0
    while ans == 0:
        reply = str(input(question+' (y/n): ')).lower().strip()
        try:
            if reply[0] == 'y':
                ans = 1
                return True
            elif reply[0] == 'n':
                ans = 1
                return False
        except IndexError:
            ans = 0

# Function to get zone/domain name
def get_zone_name(zoneid):
    zone_name = ''
    url = 'https://api.cloudflare.com/client/v4/zones/{}'.format(zoneid)
    headers = {
        'Authorization': 'Bearer {}'.format(bearer),
        'Content-Type': 'application/json',
        'Accept': 'text/plain'
        }
    r = requests.get(url, headers=headers)
    if r.status_code == 200:
        text = json.loads(r.text)
        zone_name = text["result"]["name"]
    else:
        print(r.status_code, r.text)
        sys.exit()
    return r.status_code, zone_name

# Function to add individual records via API
def add_record(zoneid,subdomain):
    url = 'https://api.cloudflare.com/client/v4/zones/{}/dns_records'.format(zoneid)
    headers = {
        'Authorization': 'Bearer {}'.format(bearer),
        'Content-Type': 'application/json',
        'Accept': 'text/plain'
        }
    payload = {
        'type':'A',
        'name':subdomain,
        'content':'1.2.3.4',
        'ttl':120,
        'proxied':False
        }
    r = requests.post(url, headers=headers, data=json.dumps(payload))
    return r.status_code, r.text

# Function to add records via API BIND import
def import_bind(zoneid,file):
    url = 'https://api.cloudflare.com/client/v4/zones/{}/dns_records/import'.format(zoneid)
    headers = {
        'Authorization': 'Bearer {}'.format(bearer),
        }
    print(url,file)
    fin = open(file, 'rb')
    files = {'file': fin}
    try:
        r = requests.post(url, headers=headers, files=files, timeout=300)
        print(r.text)
    finally:
        fin.close()
    
    return r.status_code, r.text

# Function to delete records
def del_record(zoneid,record):
    del_url = 'https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}'.format(zoneid,record)
    headers = {
        'Authorization': 'Bearer {}'.format(bearer),
        'Content-Type': 'application/json',
        'Accept': 'text/plain'
        }
    r = requests.delete(del_url, headers=headers)
    print(r.status_code,r.text)

# Function to list and extract record IDs
def list_records(zoneid):
    record_list = []
    list_url = 'https://api.cloudflare.com/client/v4/zones/{}/dns_records'.format(zoneid)
    headers = {
        'Authorization': 'Bearer {}'.format(bearer),
        'Content-Type': 'application/json',
        'Accept': 'text/plain'
        }
    r = requests.get(list_url+'?per_page=100', headers=headers)
    if r.status_code == 200:
        data = json.loads(r.text)
        result = data["result"]
        for record in result:
            record_list.append(record["id"])
    else:
        print(r.status_code,r.text)
    return record_list

# Validate zone is hex and len 32
regex = r'^[0-9a-f]{32}$'
if not re.match(regex, zoneid):
    usage()
    sys.exit()
else:
    status, zonename = get_zone_name(zoneid)

# Iterate listing because of pagination/paging
if delete_records != 0:
    record_list = list_records(zoneid)
    while len(record_list) != 0:
        for record in record_list:
            del_record(zoneid,record)
            record_list.remove(record)
        record_list = list_records(zoneid)

# Create zone records with API
if generate_records != 0 and bind == 0:
    question = 'Add {} random records to {} ?'.format(generate_records,zonename)
    prompt_ans = False
    if prompt == 1:
        prompt_ans = yes_or_no(question)
        if prompt_ans == False:
            sys.exit()
    print("\n")
    print("{: <10} {: <30} {: <20} {: <20} {: <20}".format(
        'itteration',
        'subdomain' ,
        'add time start',
        'add time end',
        'total time to add' )
        )
    characters = 'abcdefghijklmnopqrstuvwxyz'
    randstr = ''
    for i in range(0, 5):
        randstr += random.choice(characters)
    upper = int(generate_records) + 1
    for i in range(1, upper):
        subdomain = '{}.{}.{}'.format(str(i),randstr,zonename)
        addrec_start = time.time()
        status_code, text = add_record(zoneid,subdomain)
        addrec_end = time.time()
        addrec_total = addrec_end - addrec_start
        if status_code != 200:
            print(i, subdomain, addrec_start, addrec_end, addrec_total, status_code, text)
            break
        print("{: ^10} {: <30} {: <20} {: <20} {: <20}".format(i, subdomain, addrec_start, addrec_end, addrec_total))

# Create zone records in BIND file
if bind > 0:
    records_list = []
    question = 'BIND file upload {} random records to {}'.format(bind,zonename)
    prompt_ans = False
    if prompt == 1:
        prompt_ans = yes_or_no(question)
        if prompt_ans == False:
            sys.exit()
    characters = 'abcdefghijklmnopqrstuvwxyz'
    randstr = ''
    for i in range(0, 5):
        randstr += random.choice(characters)
    upper = int(bind) + 1
    for i in range(1, upper):
        subdomain = '{}.{}.{}'.format(str(i),randstr,zonename)
        records_list.append(subdomain)
    seperator = ".  1   IN  A   1.2.3.4\n"
    
    text_to_add = seperator.join(records_list) + ".  1   IN  A   1.2.3.4\n"
    bind_text = bind_template.format(zonename=zonename)
    final_text = bind_text+text_to_add
    epoch = int(round(time.time() * 1000))
    file_name = "{}_{}.txt".format(epoch,zonename)
    f = open(file_name, "a")
    f.write(final_text)
    f.close()
    addbind_start = time.time()
    status,text = import_bind(zoneid,file_name)
    addbind_end = time.time()
    addbind_total = addbind_end - addbind_start
    print("{: <40} {: <20} {: <20} {: <20}".format(file_name, addbind_start, addbind_end, addbind_total))
    if status != 200:
        print(status, text)


