# DNS Record Generator

This Python script will generate 'A' records in ascending numeric order on a Cloudflare zone. The records will follow this format:
```
{int}.{5_rand_char}.{zone_name}.  1   IN  A   1.2.3.4
```

Included is a delete function to delete all existing records. 

## Getting Started

This script uses the [Cloudflare API's](https://api.cloudflare.com/) and requires an [API token](https://blog.cloudflare.com/api-tokens-general-availability/). Note Cloudflare has an API rate limit.

Delete all records from a Cloudflare zone:
```
python3 dns_gen.py -z <zoneID> -d
```

Generate 10 records and create them on the Cloudflare zone:
```
python3 dns_gen.py -z <zoneID> -g 10
```

Upload 100 records via zone file in BIND format:
```
python3 dns_gen.py -z <zoneID> -b 100
```

usage:
```
-b --bind <integer> (generate zone files and upload)
-d --delete (deletes all dns records for given zone)
-g --generate <integer> (generate individual records using API)
-h --help
-y --yes (no prompt)
-z --zoneid <zoneId>
```

### Prerequisites

- [Cloudflare API Token](https://support.cloudflare.com/hc/en-us/articles/200167836-Managing-API-Tokens-and-Keys)
- Python3
- Pip3 
- [Requests](https://requests.kennethreitz.org/en/master/) - Module for making HTTP requests

```
sudo apt install python3
sudo apt install python3-pip
pip3 install requests
```

### Installing

Local copy of dns_gen.py 

### Authors

flow-wolf



