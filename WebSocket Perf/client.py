import os
import re
import sys
import time
import getopt
import random
import string
import datetime
import websocket
try:
    import thread
except ImportError:
    import _thread as thread
 
 
# Initialise variables
port = ''
repeat = 10
size = 0
data = 0
domain = ''
wait = 1
 
 
# Define script usage
def usage():
    print('usage:')
    print('-d --domain <char> (domain, subdomain, or IP)')
    print('-p --port <integer> (set the port number, defaults to 80 if not specified)')
    print('-r --repeat <integer> (number of times to send the message, defaults to 10 if not specified)')
    print('-s --size <integer> (message size in bytes)')
    print('-w --wait <integer> (time interval between messages in seconds, defaults to 1 if not specified)')
    print('-h --help')
    sys.exit()
 
 
# Get user input
try:
    opts, args = getopt.getopt(sys.argv[1:], 'd:r:s:p:w:h', ['domain=', 'repeat', 'size=', 'port=', 'wait=' ,'help'])
except getopt.GetoptError:
    usage()
 
for opt, arg in opts:
    if opt in ('-h', '--help'):
        usage()
    elif opt in ('-r', '--repeat'):
        try:
            repeat = int(arg)
        except:
            usage()
    elif opt in ('-w', '--wait'):
        try:
            wait = int(arg)
        except:
            usage()
    elif opt in ('-s', '--size'):
        try:
            size = int(arg)
        except:
            usage()
    elif opt in ('-d', '--domain'):
        domain = arg
    elif opt in ('-p', '--port'):
        port = arg
    else:
        usage()
 
# Validate user input of IP or URL
ip_pattern = r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'
url_pattern = r'^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$'
if not re.search(url_pattern,domain) and not re.search(ip_pattern,domain):
    usage()
 
 
# Generate data if defined by user
def gen_chars(size):
    letters = string.ascii_lowercase
    chars = ''.join(random.choice(letters) for i in range(size))
    return chars
if size > 0:
    data = gen_chars(size)
 
 
# Generate file to save results to
x = datetime.datetime.now()
dateString = x.strftime("%Y-%m-%d_%H:%M")
fileName = "results_" + dateString + ".csv"
f = open(fileName, "a")
f.write("\nmsg_id,sent,recvd\n")
 
 
 
def on_message(ws, message):
    print(message)
    if "opened" not in message:
        chunks = message.split(" ")
        line = ""
        for chunk in chunks:
            if chunk:
                line = line + str(chunk) + ","
        line = line[:-1]
        f.write("{}\n".format(line))
 
def on_error(ws, error):
    print(error)
    f.close()
 
def on_close(ws):
    print("{}\n{}".format("="*50,"### closed ###"))
    f.close()
 
def on_open(ws):
    def run(*args):
        for i in range(repeat):
            epoch = time.time()
            len_i = len(str(i))
            len_epoch = len(str(epoch))
            len_to_subtract = len_i + len_epoch + 2
            modified_data = data[:-len_to_subtract]
            # print("{} {} {} {}".format(len_i, len_epoch, len_data, len_to_subtract))
            # print("{}:{}:{}".format(i,epoch,modified_data))
            ws.send( "{}:{}:{}".format(i,epoch,modified_data) )
            time.sleep(wait)
        ws.close()
        print("thread terminating...")
    thread.start_new_thread(run, ())
 
 
if __name__ == "__main__":
    url = "ws://{}:{}/".format(domain,port)
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(url,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
