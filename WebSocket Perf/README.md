# WebSocket Network Performance

The two scripts are designed to test how long it takes for a WebSocket message to go from client to server. The message sizes and number of times they are sent can be specified. The client will output to screen and CSV file in the same directory.

### Expected Output

```
$ python3 ws_client.py -p 80 -s 1000000 -d websocket.flow-wolf.io -r 10 -w 2
### opened ###
==================================================
msg id     sent                 recvd              
==================================================
0          1598485282.054113    1598485284.5088873 
1          1598485284.5158558   1598485286.1956215 
2          1598485286.8762207   1598485288.355367  
3          1598485289.2351031   1598485290.5448437 
4          1598485291.6256654   1598485292.7402737 
5          1598485294.0207667   1598485295.0929973 
6          1598485296.385156    1598485297.4849517 
7          1598485298.7745202   1598485299.8547502 
8          1598485301.1455832   1598485302.2394478 
9          1598485303.5348246   1598485304.6139646 
==================================================
### closed ###
thread terminating...
```

### Prerequisites
- Python3
- Pip3 
- [websocket](https://websocket-client.readthedocs.io/en/latest/) - Module for for WebSocket client
- [tornado](https://www.tornadoweb.org/en/stable/) - Module for WebSocket server

```
sudo apt install python3
sudo apt install python3-pip
pip3 install websocket
pip3 install websocket_client
pip3 install tornado
```

## Getting Started

Server: 
The server runs on port 80
```
$ python3 server.py
```

Client:
Open a connection to server on port 80, message size of 1,000,000 bytes, domain websocket.flow-wolf.io, repeat sending of message 100 time, with a wait of 5 seconds between each send:
```
$ python3 client.py -p 80 -s 1000000 -d websocket.flow-wolf.io -r 100 -w 5
```

usage:
```
 -d --domain <char> (domain, subdomain, or IP)
 -p --port <integer> (set the port number, defaults to 80 if not specified)
 -r --repeat <integer> (number of times to send the message, defaults to 10 if not specified)
 -s --size <integer> (message size in bytes)
 -w --wait <integer> (time interval between messages in seconds, defaults to 1 if not specified)
 -h --help
```


### Authors

flow-wolf
