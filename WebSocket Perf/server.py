import time
import tornado.web
import tornado.ioloop
import tornado.httpserver
import tornado.websocket as ws
from tornado.options import define, options
 
define('port', default=80, help='port to listen on')
 
class websocket_handler(ws.WebSocketHandler):
    @classmethod
    def route_urls(cls):
        return [(r'/',cls, {}),]
     
    def simple_init(self):
        self.last = time.time()
        self.stop = False
     
    def open(self):
        self.simple_init()
        print("New client connected")
        msg = "{}\n{}\n{: <10} {: <20} {: <20}\n{}".format("### opened ###","="*50,"msg id","sent","recvd","="*50)
        self.write_message(msg)
 
    def on_message(self, message):
        epoch = time.time()
        # print("mesg: {}".format(message))
        chunks = message.split(':')
        # for chunk in chunks:
        #     print("{}".format(chunk))
        self.write_message("{: <10} {: <20} {: <20}".format(str(chunks[0]),str(chunks[1]),str(epoch)))
        self.last = time.time()
     
    def on_close(self):
        print("connection is closed")
     
    def check_origin(self, origin):
        return True
 
def start_server():
    app = tornado.web.Application(websocket_handler.route_urls())
    server = tornado.httpserver.HTTPServer(app)
    server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
 
if __name__ == '__main__':
    start_server()
