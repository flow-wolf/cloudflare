# Intro

[Credential stuffing](https://owasp.org/www-community/attacks/Credential_stuffing) is a relatively easy way for bad actors to gain access to online accounts via the use of breached usernames and passwords. While MFA is the most effective defense against this form of attack, it may not always be possible to enforce on end users.

Login Credence will offer a solution to be used in conjunction or in the absence of MFA.


## Login Credence Explained

<Place Holder>

### Prerequisites

- [A domain on Cloudflare](https://support.cloudflare.com/hc/en-us/articles/201720164-Creating-a-Cloudflare-account-and-adding-a-website)
- [Cloudflare Workers](https://developers.cloudflare.com/workers/)


### Installing

Create a Cloudflare Workers script and add the the Login Credence javascript

### Authors

flow-wolf